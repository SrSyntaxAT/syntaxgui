package at.srsyntax.spigot.sgui.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.03.2020 at 00:27. All rights reserved.
 */
public class StringHelper {

    public static String replace(String string) {
        return replace(string, null);
    }

    public static String replace(String string, HashMap<String, String> replaces) {
        if (string == null) return null;

        if (replaces == null)
            replaces = new HashMap<>();

        replaces.put("&", "§");
        replaces.put("%oe%", "ö");
        replaces.put("%OE%", "Ö");
        replaces.put("%ae%", "ä");
        replaces.put("%AE%", "Ä");
        replaces.put("%ue%", "ü");
        replaces.put("%UE%", "Ü");

        for (Map.Entry<String, String> entry : replaces.entrySet()) {
            string = string.replace(entry.getKey(), entry.getValue());
        }

        return string;
    }
}
