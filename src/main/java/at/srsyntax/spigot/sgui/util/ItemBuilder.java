package at.srsyntax.spigot.sgui.util;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by SrSyntaxAT | Marcel H. on 29.02.2020 at 20:32. All rights reserved.
 */
public class ItemBuilder {

    private final ItemStack itemStack;
    private final ItemMeta itemMeta;

    public ItemBuilder(Material material) {
        this.itemStack = new ItemStack(material);
        this.itemMeta = this.itemStack.getItemMeta();
    }

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
        this.itemMeta = this.itemStack.getItemMeta();
    }

    public ItemBuilder setMaterial(Material material) {
        itemStack.setType(material);
        return this;
    }

    public ItemBuilder setSubid(byte subid) {
        itemStack.getData().setData(subid);
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        itemStack.setAmount(amount);
        return this;
    }

    public ItemBuilder setName(String name) {
        this.itemMeta.setDisplayName(name);
        return this;
    }

    public ItemBuilder setDurability(short durability) {
        this.itemStack.setDurability(durability);
        return this;
    }

    public ItemBuilder setEnchantments(Map<Enchantment, Integer> enchantments) {
        enchantments.forEach(this.itemStack::addEnchantment);
        return this;
    }
    public ItemBuilder addEnchantment(Enchantment enchantment, Integer level) {
        this.itemStack.addEnchantment(enchantment, level);
        return this;
    }
    public ItemBuilder clearEnchantments() {
        this.itemStack.getEnchantments().keySet().forEach(this.itemStack::removeEnchantment);
        return this;
    }
    public ItemBuilder removeEnchantment(Enchantment enchantment) {
        this.itemStack.removeEnchantment(enchantment);
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        itemMeta.setLore(lore);
        return this;
    }

    public ItemBuilder addLore(String lore) {
        List<String> loreList = itemMeta.getLore();
        if (loreList == null) loreList = new ArrayList<>();
        loreList.add(lore);
        itemMeta.setLore(loreList);
        return this;
    }

    public ItemBuilder clearLore() {
        itemMeta.getLore().clear();
        return this;
    }

    public ItemBuilder removeLore(String lore) {
        itemMeta.getLore().remove(lore);
        return this;
    }

    public ItemStack build() {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
