package at.srsyntax.spigot.sgui.listener;

import at.srsyntax.spigot.sgui.SyntaxGUI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.03.2020 at 00:55. All rights reserved.
 */
public class GUIListener implements Listener {

    public GUIListener() {
        Bukkit.getServer().getPluginManager().registerEvents(this, SyntaxGUI.getInstance());
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        SyntaxGUI.getInstance().getGuis().forEach((name, gui) -> {
            if (gui.hasJoinItem())
                gui.getJoinItem().giveItem(event.getPlayer());
        });
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        final SyntaxGUI syntaxGUI = SyntaxGUI.getInstance();
        if (syntaxGUI.getGuis() != null && !syntaxGUI.getGuis().isEmpty()) {
            syntaxGUI.getGuis().forEach((name, gui) -> {
                if (gui.hasJoinItem() && gui.getJoinItem().isItem(event.getItem())) {
                    gui.getJoinItem().handle(event.getPlayer());
                    if (!gui.getJoinItem().isCanUse())
                        event.setCancelled(true);
                }
            });
        }
    }

    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        if (event.getClickedInventory() != null) {
            final SyntaxGUI syntaxGUI = SyntaxGUI.getInstance();
            if (syntaxGUI.getGuis() != null && !syntaxGUI.getGuis().isEmpty()) {
                syntaxGUI.getGuis().forEach((name, gui) -> {
                    if (gui.isGUI(event.getClickedInventory())) {
                        if (gui.getActions() != null && !gui.getActions().isEmpty()) {
                            if (gui.getActions().containsKey(event.getSlot())) {
                                gui.handle((Player) event.getWhoClicked(), event.getSlot());
                            }
                        }

                        if (!gui.isCanMove()) {
                            event.setCancelled(true);
                        }
                    }

                    if (gui.hasJoinItem()) {
                        if (gui.getJoinItem().isItem(event.getClickedInventory().getItem(event.getSlot()))) {
                            if (!gui.getJoinItem().isCanMove())
                                event.setCancelled(true);
                        }
                    }
                });
            }
        }
    }
}
