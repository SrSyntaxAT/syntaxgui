package at.srsyntax.spigot.sgui;

import at.srsyntax.spigot.sgui.gui.GUI;
import at.srsyntax.spigot.sgui.gui.JoinItem;
import at.srsyntax.spigot.sgui.gui.action.GUIAction;
import at.srsyntax.spigot.sgui.gui.action.GUIActionType;
import at.srsyntax.spigot.sgui.listener.GUIListener;
import at.srsyntax.spigot.sgui.util.ItemBuilder;
import at.srsyntax.spigot.sgui.util.StringHelper;
import at.srsyntax.spigot.sgui.exception.TypeNotFoundException;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SrSyntaxAT | Marcel H. on 29.02.2020 at 20:28. All rights reserved.
 */
public class SyntaxGUI extends JavaPlugin {

    private static SyntaxGUI instance;

    private final Map<String, GUI> guis = new HashMap<>();

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        try {
            getLogger().info("Load GUIs..");
            loadGUIs();
        } catch (IOException e) {
            e.printStackTrace();
            onDisable();
        } catch (TypeNotFoundException e) {
            e.printStackTrace();
        }

        new GUIListener();
    }

    public static SyntaxGUI getInstance() {
        return instance;
    }

    private void loadGUIs() throws IOException, TypeNotFoundException {
        if (!getDataFolder().exists()) getDataFolder().mkdirs();
        final File file = new File(getDataFolder().getPath(), "config.yml");
        if (!file.exists()) {
            file.createNewFile();

            getConfig().options().copyDefaults(true);
            getConfig().save(file);
        }

        final List<String> rawGuisList = getConfig().getStringList("guis");
        for (String gui : rawGuisList) {
            String path = gui + ".joinItem.";
            JoinItem joinItem = null;

            if (getConfig().getBoolean(path + "active")) {
                path = path + "item.";

                final Material material = Material.getMaterial(getConfig().getString(path + "material"));
                if (material == null) throw new NullPointerException();

                final ItemBuilder builder = new ItemBuilder(material);
                builder.setSubid((byte) getConfig().getInt(path + "subid"));
                builder.setAmount(getConfig().getInt(path + "amount"));

                final List<String> lore = getConfig().getStringList(path + "lore");
                if (!lore.isEmpty()) {
                    for (String line : lore) {
                        builder.addLore(StringHelper.replace(line));
                    }
                }

                List<GUIAction> actions = null;
                final List<String> rawActionsList = getConfig().getStringList(path + "actions");

                if (!rawActionsList.isEmpty()) {
                    actions = new ArrayList<>(rawActionsList.size() + 1);

                    for (final String rawAction : rawActionsList) {
                        final String[] splited = rawAction.split(";");
                        GUIActionType type;

                        try {
                            type = GUIActionType.valueOf(splited[0].split("-")[0].toUpperCase());
                        } catch (Exception exception) {
                            throw new TypeNotFoundException();
                        }

                        GUIAction action;

                        if (type == GUIActionType.BROADCAST_PERMISSION) {
                            action = new GUIAction(type, splited[1], splited[0].split("-")[1]);
                        } else if (type == GUIActionType.NOTHING || type == GUIActionType.CLOSE_GUI) {
                            action = new GUIAction(type, null);
                        } else {
                            action = new GUIAction(type, splited[1]);
                        }

                        actions.add(action);
                    }
                }

                String displayname = StringHelper.replace(getConfig().getString(path + "displayname"));
                if (displayname == null) displayname = material.name();
                builder.setName(displayname);

                joinItem = new JoinItem(
                        true,
                        displayname,
                        builder.build(),
                        getConfig().getInt(path + "slot"),
                        getConfig().getBoolean(path + "add"),
                        getConfig().getBoolean(path + "can.move"),
                        getConfig().getBoolean(path + "can.use"),
                        actions,
                        getConfig().getString(path + "permission")
                    );
            }

            path = gui + ".gui.";
            String displayname = StringHelper.replace(getConfig().getString(path + "displayname"));
            if (displayname == null) displayname = "rip no displayname";

            getGuis().put(gui, new GUI(true, gui, getConfig().getInt(path + "size"), displayname, joinItem, getConfig().getBoolean(path + "can.move")));
            getLogger().info("GUI '" + getGuis().get(gui).toString() + "' has been loaded.");
        }

        getLogger().info(getGuis().size() + " GUI(s) were loaded.");
    }

    public Map<String, GUI> getGuis() {
        return guis;
    }
}
