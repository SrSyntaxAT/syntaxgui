package at.srsyntax.spigot.sgui.gui;

import at.srsyntax.spigot.sgui.SyntaxGUI;
import at.srsyntax.spigot.sgui.gui.action.GUIAction;
import at.srsyntax.spigot.sgui.gui.action.GUIActionType;
import at.srsyntax.spigot.sgui.util.ItemBuilder;
import at.srsyntax.spigot.sgui.util.StringHelper;
import at.srsyntax.spigot.sgui.exception.TypeNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SrSyntaxAT | Marcel H. on 29.02.2020 at 20:36. All rights reserved.
 */
public class GUI {

    private final boolean active;

    private final String name;
    private final int size;
    private final String displayname;
    private final JoinItem joinItem;
    private final boolean canMove;
    private Map<Integer, List<GUIAction>> actions;

    public GUI(boolean active, String name, int size, String displayname, JoinItem joinItem, boolean canMove) {
        this.active = active;
        this.name = name;
        this.size = size;
        this.displayname = displayname;
        this.joinItem = joinItem;
        this.canMove = canMove;
    }

    public Inventory createInventory() throws TypeNotFoundException {
        final Inventory inventory = Bukkit.createInventory(null, size, displayname);
        final FileConfiguration config = SyntaxGUI.getInstance().getConfig();
        String path = name + ".gui.";

        final boolean fill = config.getBoolean(path + "fill");
        ItemStack fillItem = null;
        if (fill)
            fillItem = new ItemBuilder(Material.getMaterial(config.getString(path + "fillItem.material")))
                    .setAmount(config.getInt(path + "fillItem.amount"))
                    .setSubid((byte) config.getInt(path + "fillItem.subid"))
                    .setName(" ").build();

        path = path + "slots.";

        for (int i = 0; i < size; i++) {
            String newPath = path + (i + 1) + ".";

            if (config.get(path + (i + 1)) == null) {
                if (fill) {
                    inventory.setItem(i, fillItem);
                }
            } else {
                final ItemBuilder item = new ItemBuilder(Material.getMaterial(config.getString(newPath + "material")))
                        .setName(StringHelper.replace(config.getString(newPath + "displayname")))
                        .setAmount(config.getInt(newPath + "amount"))
                        .setSubid((byte) config.getInt(newPath + "subid"));
                inventory.setItem(i, item.build());
            }

            final List<String> rawActions = config.getStringList(newPath + "actions");
            final List<GUIAction> actions;

            if (!rawActions.isEmpty()) {
                actions = new ArrayList<>(rawActions.size());

                for (String rawAction : rawActions) {
                    String[] splited = rawAction.split(";");

                    GUIActionType type;

                    try {
                        type = GUIActionType.valueOf(splited[0].split("-")[0].toUpperCase());
                    } catch (Exception exception) {
                        throw new TypeNotFoundException();
                    }

                    GUIAction action;

                    if (type == GUIActionType.BROADCAST_PERMISSION) {
                        action = new GUIAction(type, splited[1], splited[0].split("-")[1]);
                    } else if (type == GUIActionType.NOTHING || type == GUIActionType.CLOSE_GUI) {
                        action = new GUIAction(type, null);
                    } else {
                       action = new GUIAction(type, splited[1]);
                    }

                    actions.add(action);
                }

                if (this.actions == null) this.actions = new HashMap<>();
                getActions().put(i, actions);
            }
        }

        return inventory;
    }

    public void handle(Player player, int slot) {
        if (actions != null && !actions.isEmpty() && actions.containsKey(slot)) {
            for (GUIAction action : actions.get(slot)) {
                switch (action.getType()) {
                    case OPEN_GUI:
                        player.closeInventory();
                        try {
                            SyntaxGUI.getInstance().getGuis().get(action.getValue()).openGUI(player);
                        } catch (TypeNotFoundException e) {
                            player.sendMessage("§cGUI can't be load.");
                            e.printStackTrace();
                        }
                        break;
                    case BROADCAST:
                        Bukkit.getServer().broadcastMessage(StringHelper.replace(action.getValue()));
                        break;
                    case BROADCAST_PERMISSION:
                        Bukkit.getServer().broadcast(StringHelper.replace(action.getValue()), action.getPermission());
                        break;
                    case MESSAGE:
                        player.sendMessage(StringHelper.replace(action.getValue()));
                        break;
                    case COMMAND_PLAYER:
                        Bukkit.getServer().dispatchCommand(player, action.getValue());
                        break;
                    case COMMAND_CONSOLE:
                        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), action.getValue());
                        break;
                    case CLOSE_GUI:
                        player.closeInventory();
                        break;
                    default: break;
                }
            }
        }
    }

    public boolean isGUI(Inventory inventory) {
        return inventory.getName().equals(displayname);
    }

    public void openGUI(Player player) throws TypeNotFoundException {
        player.openInventory(createInventory());
    }

    public boolean hasJoinItem() {
        return joinItem != null;
    }

    public boolean isActive() {
        return active;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public JoinItem getJoinItem() {
        return joinItem;
    }

    public String getDisplayname() {
        return displayname;
    }

    public boolean isCanMove() {
        return canMove;
    }

    public Map<Integer, List<GUIAction>> getActions() {
        return actions;
    }

    @Override
    public String toString() {
        return "GUI{" +
                "active=" + active +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", displayname='" + displayname + '\'' +
                ", joinItem=" + joinItem.toString() +
                ", actions=" + actions +
                '}';
    }
}
