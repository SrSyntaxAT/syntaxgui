package at.srsyntax.spigot.sgui.gui;

import at.srsyntax.spigot.sgui.SyntaxGUI;
import at.srsyntax.spigot.sgui.gui.action.GUIAction;
import at.srsyntax.spigot.sgui.util.StringHelper;
import at.srsyntax.spigot.sgui.exception.TypeNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.03.2020 at 00:13. All rights reserved.
 */
public class JoinItem {

    private final boolean active;

    private final String displayname;
    private final ItemStack item;
    private final int slot;
    private final boolean add;
    private final boolean canMove;
    private final boolean canUse;
    private final List<GUIAction> actions;
    private final String permission;

    public JoinItem(boolean active, String displayname, ItemStack item, int slot, boolean add, boolean canMove, boolean canUse, List<GUIAction> actions, String permission) {
        this.active = active;
        this.displayname = displayname;
        this.item = item;
        this.slot = slot;
        this.add = add;
        this.canMove = canMove;
        this.canUse = canUse;
        this.actions = actions;
        this.permission = permission;
    }

    public void giveItem(Player player) {
        if (permission != null && !player.hasPermission(permission)) return;
        if (item != null) {
            if (add) {
                player.getInventory().addItem(item);
            } else {
                player.getInventory().setItem((slot - 1), item);
            }
        }
    }

    public boolean isItem(ItemStack item) {
        return getItem().equals(item);
    }

    public void handle(Player player) {
        if (actions != null && !actions.isEmpty()) {
            for (final GUIAction action : actions) {
                switch (action.getType()) {
                    case OPEN_GUI:
                        try {
                            SyntaxGUI.getInstance().getGuis().get(action.getValue()).openGUI(player);
                        } catch (TypeNotFoundException e) {
                            player.sendMessage("§cGUI can't be load.");
                            e.printStackTrace();
                        }
                        break;
                    case BROADCAST:
                        Bukkit.getServer().broadcastMessage(StringHelper.replace(action.getValue()));
                        break;
                    case BROADCAST_PERMISSION:
                        Bukkit.getServer().broadcast(StringHelper.replace(action.getValue()), action.getPermission());
                        break;
                    case MESSAGE:
                        player.sendMessage(StringHelper.replace(action.getValue()));
                        break;
                    case COMMAND_PLAYER:
                        Bukkit.getServer().dispatchCommand(player, action.getValue());
                        break;
                    case COMMAND_CONSOLE:
                        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), action.getValue());
                        break;
                    case NOTHING: break;
                    default: break;
                }
            }
        }
    }

    public boolean isCanMove() {
        return canMove;
    }

    public boolean isCanUse() {
        return canUse;
    }

    public boolean isActive() {
        return active;
    }

    public String getDisplayname() {
        return displayname;
    }

    public ItemStack getItem() {
        return item;
    }

    public List<GUIAction> getActions() {
        return actions;
    }

    @Override
    public String toString() {
        return "JoinItem{" +
                "active=" + active +
                ", displayname='" + displayname + '\'' +
                ", item=" + item +
                ", slot=" + slot +
                ", add=" + add +
                ", canMove=" + canMove +
                ", canUse=" + canUse +
                ", actions=" + actions +
                ", permission='" + permission + '\'' +
                '}';
    }
}
