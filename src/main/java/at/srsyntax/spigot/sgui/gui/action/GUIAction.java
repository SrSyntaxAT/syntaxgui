package at.srsyntax.spigot.sgui.gui.action;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.03.2020 at 00:11. All rights reserved.
 */
public class GUIAction {

    private final GUIActionType type;
    private final String value;
    private final String permission;

    public GUIAction(GUIActionType type, String value) {
        this.type = type;
        this.value = value;
        this.permission = null;
    }

    public GUIAction(GUIActionType type, String value, String permission) {
        this.type = type;
        this.value = value;
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

    public GUIActionType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "GUIAction{" +
                "type=" + type +
                ", value='" + value + '\'' +
                ", permission='" + permission + '\'' +
                '}';
    }
}
