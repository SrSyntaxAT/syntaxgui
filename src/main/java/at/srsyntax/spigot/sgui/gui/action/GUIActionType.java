package at.srsyntax.spigot.sgui.gui.action;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.03.2020 at 00:08. All rights reserved.
 */
public enum GUIActionType {
    BROADCAST,
    BROADCAST_PERMISSION,
    COMMAND_PLAYER,
    COMMAND_CONSOLE,
    MESSAGE,
    NOTHING,
    OPEN_GUI,
    CLOSE_GUI
}
